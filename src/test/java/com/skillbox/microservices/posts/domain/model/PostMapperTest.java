package com.skillbox.microservices.posts.domain.model;

import com.skillbox.microservices.posts.domain.entity.Post;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PostMapperTest {
    private final ModelMapper modelMapper = new ModelMapper();
    private final PostMapper postMapper = new PostMapper(modelMapper);

    @Test
    void toEntityTest() {
        PhotoDto photoDto = PhotoDto.builder()
                .id(1L)
                .name("asd.jpeg")
                .link("http://minio.link/posts/asd.jpeg")
                .build();
        PhotoDto photoDto2 = PhotoDto.builder()
                .id(1L)
                .name("asd2.jpeg")
                .link("http://minio.link/posts/asd2.jpeg")
                .build();
        List<PhotoDto> photos = new ArrayList<>();
        photos.add(photoDto);
        photos.add(photoDto2);
        PostDto postDto = PostDto.builder()
                .id(1L)
                .title("Пост 1")
                .userId(1L)
                .descriptions("Описание поста 1")
                .photos(photos)
                .build();
        Post post = postMapper.toEntity(postDto);
        assertEquals(post.getTitle(), postDto.getTitle());

    }

}