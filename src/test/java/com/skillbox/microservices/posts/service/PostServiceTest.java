package com.skillbox.microservices.posts.service;

import com.skillbox.microservices.posts.domain.entity.Post;
import com.skillbox.microservices.posts.domain.model.PostDto;
import com.skillbox.microservices.posts.domain.model.PostMapper;
import com.skillbox.microservices.posts.repository.PostRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class PostServiceTest {

    private final PostRepository postRepository = mock(PostRepository.class);
    ModelMapper modelMapper = new ModelMapper();
    PostMapper postMapper = new PostMapper(modelMapper);
    PostService postService = new PostService(postRepository, postMapper);
    PostDto postDto;
    @BeforeEach
    void init() {
        postDto = PostDto.builder()
                .id(1L)
                .title("title")
                .descriptions("description")
                .photos(new ArrayList<>())
                .build();
    }

    @Test
    void createPostTest() {
        Post savedPost = postMapper.toEntity(postDto);
        when(postRepository.save(any(Post.class))).thenReturn(savedPost);
        String actual = postService.createPost(postDto);
        String expected = String.format("Пост %s добавлен с id = %s",
                savedPost.getTitle(), savedPost.getId());
        assertEquals(expected, actual);
    }

    @Test
    void createPostTestFail() {
        when(postRepository.save(any(Post.class))).thenThrow(IllegalArgumentException.class);
        Executable executable = () -> postService.createPost(postDto);
        assertThrows(ResponseStatusException.class, executable);
    }

    @Test
    void getPostTest() {
        long postId = 1L;
        Post entity = postMapper.toEntity(postDto);
        when(postRepository.findById(postId)).thenReturn(Optional.of(entity));
        PostDto dto = postService.getPost(postId);
        assertAll(
                () -> assertEquals(entity.getTitle(), dto.getTitle()),
                () -> assertEquals(entity.getDescriptions(), dto.getDescriptions())
        );
    }

    @Test
    void getPostTestFail() {
        long postId = 1L;
        when(postRepository.findById(postId)).thenThrow(
                new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Пост с id = %s не найден", postId))
        );
        Executable executable = () -> postService.getPost(postId);
        ResponseStatusException responseStatusException = assertThrows(ResponseStatusException.class, executable);
        String expected = "404 NOT_FOUND \"Пост с id = 1 не найден\"";
        assertEquals(expected, responseStatusException.getMessage());
    }
    @Test
    void deletePostTest() {
        long postId = 1L;
        doNothing().when(postRepository).deleteById(postId);
        String expected = "Пост с id = 1 успешно удален";
        String actual = postService.deletePost(postId);
        assertEquals(expected, actual);
    }

    @Test
    void deletePostTestFail() {
        long postId = 1L;
        doThrow(new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("Не удалось удалить пост с id = %s", postId)))
                .when(postRepository).deleteById(postId);
        String expected = "400 BAD_REQUEST \"Не удалось удалить пост с id = 1\"";
        Executable executable = () -> postService.deletePost(postId);
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, executable);
        assertEquals(expected, exception.getMessage());
    }

}