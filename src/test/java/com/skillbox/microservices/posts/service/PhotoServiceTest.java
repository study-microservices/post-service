package com.skillbox.microservices.posts.service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.skillbox.microservices.posts.domain.entity.Photo;
import com.skillbox.microservices.posts.domain.entity.Post;
import com.skillbox.microservices.posts.properties.S3Properties;
import com.skillbox.microservices.posts.repository.PhotoRepository;
import com.skillbox.microservices.posts.repository.PostPhotoRepository;
import com.skillbox.microservices.posts.repository.PostRepository;
import io.findify.s3mock.S3Mock;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PhotoServiceTest {

    @Mock
    S3Properties s3Properties;
    @Mock
    MultipartFile multipartFile;

    S3Mock s3Mock;
    AmazonS3Client amazonS3Client;
    @Mock
    PhotoRepository repository;
    @Mock
    PostRepository postRepository;
    @Mock
    PostPhotoRepository postPhotoRepository;
    PhotoService photosService;
    private final String bucketName = "test-bucket";
    private final String fileName = "test-file.jpg";

    @BeforeEach
    void initMocks() throws IOException {
        this.s3Mock = new S3Mock.Builder()
                .withPort(8082)
                .withInMemoryBackend()
                .build();
        s3Mock.start();

        AwsClientBuilder.EndpointConfiguration endpoint =
                new AwsClientBuilder.EndpointConfiguration("http://localhost:8082", "by-center");
        this.amazonS3Client = (AmazonS3Client) AmazonS3ClientBuilder
                .standard()
                .withPathStyleAccessEnabled(true)
                .withEndpointConfiguration(endpoint)
                .withCredentials(new AWSStaticCredentialsProvider(new AnonymousAWSCredentials()))
                .build();

        lenient().when(s3Properties.getBucketPhotos()).thenReturn(bucketName);
        lenient().when(multipartFile.getBytes()).thenReturn("test".getBytes());
        this.photosService = new PhotoService(repository, postRepository, postPhotoRepository, s3Properties);
    }

    @AfterEach
    void tearDown() {
        s3Mock.shutdown();
    }

    @Test
    void testUploadPhotoSuccess() throws IOException {
        amazonS3Client.createBucket(bucketName);
        Long postId = 1L;
        Post post = Post.builder()
                .id(1L)
                .title("")
                .descriptions("")
                .photoLinks(new HashSet<>())
                .build();
        when(postRepository.findById(1L)).thenReturn(Optional.of(post));

        String actual = photosService.uploadPhoto(postId, fileName, multipartFile);
        String expected = "Файл test-file.jpg успешно загружен";
        assertEquals(expected, actual);
    }

    @Test
    void testUploadPhotoFail() throws IOException {
        Long postId = 1L;
        when(postRepository.findById(1L))
                .thenThrow(ResponseStatusException.class);
        Executable executable = () -> photosService.uploadPhoto(postId, fileName, multipartFile);
        assertThrows(ResponseStatusException.class, executable);
    }

    @Test
    void testDeletePhotoSuccess() throws IOException {
        amazonS3Client.createBucket(bucketName);
        Long photoId = 1L;
        String fileName = "test-file.jpg";
        Photo photo = Photo.builder()
                .id(1L)
                .name("test-file.jpg")
                .build();
        when(postPhotoRepository.findById(photoId)).thenReturn(Optional.of(photo));
        S3Object s3Object;
        s3Object = new S3Object();
        s3Object.setKey("");
        s3Object.setBucketName("");
        when(repository.get(fileName)).thenReturn(Optional.of(s3Object));
        String actual = photosService.deletePhoto(photoId);
        String expected = "Файл test-file.jpg успешно удален";
        assertEquals(expected, actual);
    }

    @Test
    void testDeletePhotoFail() {
        String expected = "400 BAD_REQUEST \"Фото id=2 не найдено\"";
        Long photoId = 2L;
        when(postPhotoRepository.findById(photoId))
                .thenThrow(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Фото id=2 не найдено"));
        Executable executable = () -> photosService.deletePhoto(photoId);
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, executable);
        String actual = String.format("%s", exception.getMessage());
        assertEquals(expected, actual);
    }

}