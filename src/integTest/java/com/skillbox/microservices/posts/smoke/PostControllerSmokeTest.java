package com.skillbox.microservices.posts.smoke;

import com.amazonaws.services.customerprofiles.model.Gender;
import com.skillbox.microservices.posts.containers.PostgresContainerWrapper;
import com.skillbox.microservices.posts.domain.entity.Photo;
import com.skillbox.microservices.posts.domain.model.PhotoDto;
import com.skillbox.microservices.posts.domain.model.PostDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("integTest")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
public class PostControllerSmokeTest {

    public static final String HOST_URL = "http://localhost:%s";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Container
    private static final PostgreSQLContainer<PostgresContainerWrapper> postgresContainer = new PostgresContainerWrapper();

    @DynamicPropertySource
    public static void initSystemParams(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgresContainer::getUsername);
        registry.add("spring.datasource.password", postgresContainer::getPassword);
    }

    @Test
    void createPostTest() {
        String url = String.format(HOST_URL, port);
        PhotoDto photo = PhotoDto.builder()
                .name("photo_1.jpeg")
                .link("http://minio/backet/photo_1.jpeg")
                .build();
        PostDto postDto = PostDto.builder()
                .title("title")
                .descriptions("description")
                .userId(1L)
                .build();

        ResponseEntity<String> response = restTemplate.postForEntity(url + "/posts/", postDto, String.class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response.getBody());

        String postId = response.getBody().split("=")[1].trim();

        ResponseEntity<PostDto> getResponse = restTemplate.getForEntity(url + "/posts/" + postId, PostDto.class);
        assertEquals(200, getResponse.getStatusCodeValue());
        postDto.setId(Long.parseLong(postId));
        assertEquals(postDto, getResponse.getBody());

    }

}
