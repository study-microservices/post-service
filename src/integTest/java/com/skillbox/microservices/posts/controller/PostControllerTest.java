package com.skillbox.microservices.posts.controller;

import com.skillbox.microservices.posts.containers.PostgresContainerWrapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.server.ResponseStatusException;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static com.skillbox.microservices.posts.utils.TestUtils.classpathFileToString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("integTest")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
class PostControllerTest {

    @Container
    private static final PostgreSQLContainer<PostgresContainerWrapper> postgresContainer = new PostgresContainerWrapper();

    @DynamicPropertySource
    public static void initSystemParams(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgresContainer::getUsername);
        registry.add("spring.datasource.password", postgresContainer::getPassword);
    }

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @BeforeEach
    private void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void createPost() throws Exception {
        String request = classpathFileToString("/mvc-requests/create_post.json");
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/posts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
        );
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",
                        equalTo("Пост Имя поста добавлен с id = 4")));
    }

    @Test
    void createPostIfExists() throws Exception {
        String request = classpathFileToString("/mvc-requests/create_post_exists.json");
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/posts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
        );
        resultActions.andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ResponseStatusException));
    }

    @Test
    void getPost() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/posts/1")
        );
        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", equalTo("Пост 1")))
                .andExpect(jsonPath("$.descriptions", equalTo("Описание поста 1")));
    }

    @Test
    void deletePost() throws Exception {
        long postId = 2;
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete("/posts/" + postId)
        );
        resultActions
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$", equalTo("Пост с id = " + postId + " успешно удален")));
    }

    @Test
    void deletePostFail() throws Exception {
        long postId = 5;
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete("/posts/" + postId)
        );
        String expected = String.format("400 BAD_REQUEST \"%s\"", "Не удалось удалить пост с id = " + postId);
        resultActions
                .andExpect(status().is4xxClientError())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> assertEquals(expected, result.getResolvedException().getMessage()));
    }

}