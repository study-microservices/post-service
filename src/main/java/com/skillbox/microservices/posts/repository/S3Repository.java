package com.skillbox.microservices.posts.repository;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import lombok.RequiredArgsConstructor;

import java.io.InputStream;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class S3Repository {
    private final AmazonS3 s3Client;
    private final String bucketName;

    /**
     * Метод возвращает список ключей по префиксу из bucket
     * @param prefix
     * @return
     */
    public Collection<String> listKeys(String prefix) {
        return s3Client.listObjectsV2(bucketName, prefix)
                .getObjectSummaries().stream()
                .map(S3ObjectSummary::getKey)
                .collect(Collectors.toList());
    }

    /**
     * Метод возвращает коллекцию метаданных по объектам
     * @param prefix
     * @return
     */
    public Collection<S3ObjectSummary> listObjects(String prefix) {
        return s3Client.listObjectsV2(bucketName, prefix)
                .getObjectSummaries();
    }

    /**
     * Метод удаляет объект из bucket
     * @param key
     */
    public void delete(String key) {
        s3Client.deleteObject(bucketName, key);
    }

    /**
     * Метод создает объект в bucket
     * @param key
     * @param inputStream
     * @param metadata
     */
    public void put(String key, InputStream inputStream, ObjectMetadata metadata) {
        s3Client.putObject(bucketName, key, inputStream, metadata);
    }

    /**
     * Метод возвращает объект по ключу
     * @param key
     * @return
     */
    public Optional<S3Object> get(String key) {
        try {
            return Optional.of(s3Client.getObject(bucketName, key));
        } catch (AmazonServiceException exception) {
            return Optional.empty();
        }
    }

    public String getUrl(String key) {
        return s3Client.getUrl(bucketName, key).toString();
    }

}
