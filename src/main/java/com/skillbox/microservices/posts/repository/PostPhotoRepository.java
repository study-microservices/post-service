package com.skillbox.microservices.posts.repository;

import com.skillbox.microservices.posts.domain.entity.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostPhotoRepository extends JpaRepository<Photo, Long> {
}
