package com.skillbox.microservices.posts.repository;

import com.amazonaws.services.s3.AmazonS3;
import com.skillbox.microservices.posts.properties.S3Properties;
import org.springframework.stereotype.Component;

@Component
public class PhotoRepository extends S3Repository {

    public PhotoRepository(AmazonS3 s3Client, S3Properties properties) {
        super(s3Client, properties.getBucketPhotos());
    }

}
