package com.skillbox.microservices.posts.service;

import com.skillbox.microservices.posts.domain.entity.Post;
import com.skillbox.microservices.posts.domain.model.PostDto;
import com.skillbox.microservices.posts.domain.model.PostMapper;
import com.skillbox.microservices.posts.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@RequiredArgsConstructor
public class PostService {
    public static final Logger log = LoggerFactory.getLogger(PostService.class);
    private final PostRepository postRepository;
    private final PostMapper postMapper;

    public String createPost(PostDto postDto) {
        log.info("Создание поста {}", postDto);
        if (postDto.getId() != null) {
            checkPostExists(postDto.getId());
        }
        Post post = postMapper.toEntity(postDto);
        try {
            Post savedPost = postRepository.save(post);
            return String.format("Пост %s добавлен с id = %s",
                    savedPost.getTitle(), savedPost.getId());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Не удалось добавить пост: %s", ex.getMessage()));
        }
    }

    public PostDto getPost(Long id) {
        log.info("Получение поста по id={}", id);
        Post post = postRepository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Пост с id = %s не найден", id))
        );
        return postMapper.toDto(post);
    }

    public String deletePost(Long id) {
        log.info("Удаление поста по id={}", id);
        try {
            postRepository.deleteById(id);
            return String.format("Пост с id = %s успешно удален", id);
        } catch(Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Не удалось удалить пост с id = %s", id));
        }
    }

    private void checkPostExists(long postId) {
        if (postRepository.existsById(postId)) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Пост уже существует"
            );
        }
    }

}
