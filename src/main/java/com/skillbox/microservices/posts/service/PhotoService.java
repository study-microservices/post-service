package com.skillbox.microservices.posts.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.skillbox.microservices.posts.domain.entity.Photo;
import com.skillbox.microservices.posts.domain.entity.Post;
import com.skillbox.microservices.posts.exception.PostServiceCustomException;
import com.skillbox.microservices.posts.properties.S3Properties;
import com.skillbox.microservices.posts.repository.PhotoRepository;
import com.skillbox.microservices.posts.repository.PostPhotoRepository;
import com.skillbox.microservices.posts.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.sleuth.Span;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

@Service
@RequiredArgsConstructor
public class PhotoService {
    public static final Logger log = LoggerFactory.getLogger(PhotoService.class);
    private final PhotoRepository photosRepository;
    private final PostRepository postRepository;
    private final PostPhotoRepository postPhotoRepository;
    private final S3Properties properties;

    /**
     * Метод загружает файл в хранилище
     * @param postId
     * @param fileName
     * @param file
     * @return результат загрузки фото
     * @throws IOException исключение
     */
    @NewSpan
    @Transactional
    public String uploadPhoto(Long postId, String fileName, MultipartFile file) throws IOException {
        Post post = postRepository.findById(postId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Фото id=%s не найдено", postId))
        );
        String bucketPhotos = properties.getBucketPhotos();
        log.info("Загрузка файла {} в хранилище {}", fileName , bucketPhotos);
        try (InputStream stream = new ByteArrayInputStream(file.getBytes())) {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(MediaType.MULTIPART_FORM_DATA_VALUE);
            metadata.setContentLength(file.getSize());
            photosRepository.put(fileName, stream, metadata);
            Photo photo = Photo.builder()
                    .name(fileName)
                    .link(photosRepository.getUrl(fileName))
                    .post(post)
                    .build();
            postPhotoRepository.save(photo);
        } catch (AmazonServiceException e) {
            log.error("Ошибка загрузки файла: {}", e.getErrorMessage());
            return String.format("Не удалось загрузить файл %s", fileName);
        } catch (RuntimeException ex) {
            throw new PostServiceCustomException(ex.getMessage());
        }
        log.info("Файл {} успешно загружен", fileName);
        return String.format("Файл %s успешно загружен", fileName);
    }

    /**
     * Метод удаляет файл из хранилища
     * @param photoId id файла
     * @return результат выполнения
     */
    @NewSpan
    @Transactional
    public String deletePhoto(Long photoId) {
        Photo photo = postPhotoRepository.findById(photoId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Фото id=%s не найдено", photoId))
        );
        String fileName = photo.getName();
        String bucketName = properties.getBucketPhotos();
        log.info("Удаление файла {} из хранилища {}", fileName, bucketName);
        S3Object s3Object = photosRepository.get(fileName).orElse(null);
        if (s3Object == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Файл %s не найден", fileName));
        }
        try {
            photosRepository.delete(fileName);
            postPhotoRepository.deleteById(photoId);
        } catch (AmazonServiceException ex) {
            log.error("Ошибка удаления файла {}", ex.getErrorMessage());
            return String.format("Не удалось удалить файл %s", fileName);
        } catch (RuntimeException ex) {
            throw new PostServiceCustomException(ex.getMessage());
        }
        log.info("Файл {} успешно удален", fileName);
        return String.format("Файл %s успешно удален", fileName);
    }


}
