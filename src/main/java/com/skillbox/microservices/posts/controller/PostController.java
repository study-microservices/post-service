package com.skillbox.microservices.posts.controller;

import com.skillbox.microservices.posts.domain.model.PostDto;
import com.skillbox.microservices.posts.exception.PostServiceCustomException;
import com.skillbox.microservices.posts.service.PhotoService;
import com.skillbox.microservices.posts.service.PostService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.sleuth.Span;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/posts")
public class PostController {
    public static final Logger log = LoggerFactory.getLogger(PostController.class);
    private final PostService postService;
    private final PhotoService photosService;
    private final Tracer tracer;

    @PostMapping
    @Operation(summary = "Создание поста")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Пост {title} успешно создан"),
                    @ApiResponse(responseCode = "400", description = "Не удалось создать пост {title}")
            }
    )
    public String createPost(@RequestBody PostDto postDto) {
        log.info("POST>> Создание поста {}", postDto);
        return postService.createPost(postDto);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получение поста")
    @ApiResponse(responseCode = "200")
    public PostDto getPost(@PathVariable Long id) {
        log.info("GET>> Получение поста {}", id);
        return postService.getPost(id);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление поста")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Пост id={id} успешно удален"),
                    @ApiResponse(responseCode = "400", description = "Пост id={id} не найден")
            }
    )
    public String deletePost(@PathVariable Long id) {
        log.info("DELETE>> Удаление поста {}", id);
        return postService.deletePost(id);
    }

    @PostMapping("/{id}/photos")
    @Operation(summary = "Загрузка файлов")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Файл {file} успешно загружен"),
                    @ApiResponse(responseCode = "400", description = "Не удалось загрузить файл {file}")
            }
    )
    public String uploadPhoto(@PathVariable Long id, @RequestParam("file") MultipartFile file) throws IOException {
        String fileName = file.getOriginalFilename();
        log.info("POST>> Загрузка файла {}", fileName);
        return photosService.uploadPhoto(id, fileName, file);
    }

    @DeleteMapping("/{id}/photos/{photo_id}")
    @Operation(summary = "Удаление фото")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Файл {file} успешно удален"),
                    @ApiResponse(responseCode = "400", description = "Файл {file} не найден")
            }
    )
    public String deletePhoto(@PathVariable("id") Long postId, @PathVariable("photo_id") Long id) {
        log.info("DELETE>> Удаление фото по id={}", id);
        return photosService.deletePhoto(id);
    }

    @ExceptionHandler(value = {PostServiceCustomException.class})
    public ResponseEntity<String> handleException(PostServiceCustomException e) {
        Span currentSpan = tracer.currentSpan();
        String traceId = "";
        if (currentSpan != null) {
            traceId = currentSpan.context().traceId();
        }
        log.error("ERROR>> traceId={} \n{}", traceId, e.getLocalizedMessage());
        return new ResponseEntity<>(
                e.getLocalizedMessage(), HttpStatus.BAD_REQUEST
        );
    }

}