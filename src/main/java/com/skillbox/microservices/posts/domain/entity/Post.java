package com.skillbox.microservices.posts.domain.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Builder
@Getter
@Setter
@Entity
@Table(name = "posts", schema = "users_scheme")
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Post {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "Уникальный идентификатор поста", example = "1")
    Long id;
    @Column(nullable = false)
    String title;
    @Column(name = "user_id", nullable = false)
    Long userId;
    String descriptions;
    @OneToMany(mappedBy = "post")
    @JsonIgnore
    @Builder.Default
    Set<Photo> photoLinks = new HashSet<>();

}
