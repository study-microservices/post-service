package com.skillbox.microservices.posts.domain.model;

import com.skillbox.microservices.posts.domain.entity.Post;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PostMapper {
    private final ModelMapper modelMapper;

    public Post toEntity(PostDto postDto) {
        return modelMapper.map(postDto, Post.class);
    }

    public PostDto toDto(Post post) {
        return modelMapper.map(post, PostDto.class);
    }
}
