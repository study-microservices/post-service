package com.skillbox.microservices.posts.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PostDto {
    Long id;
    String title;
    @JsonProperty("user_id")
    Long userId;
    String descriptions;
    List<PhotoDto> photos;
}
