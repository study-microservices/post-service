package com.skillbox.microservices.posts.domain.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PhotoDto {
    Long id;
    String link;
    String name;
}
