package com.skillbox.microservices.posts.exception;

public class PostServiceCustomException extends RuntimeException {
    public PostServiceCustomException(String message) {
        super(message);
    }

    public PostServiceCustomException(String message, Throwable cause) {
        super(message, cause);
    }

    public PostServiceCustomException(Throwable cause) {
        super(cause);
    }

    protected PostServiceCustomException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
