plugins {
	id 'java'
	id 'org.springframework.boot' version '2.7.10'
	id 'io.spring.dependency-management' version '1.0.15.RELEASE'
	id 'com.google.cloud.tools.jib' version '3.3.0'
	id 'nebula.integtest' version '9.6.2'
	id 'jacoco'
	id "org.sonarqube" version "4.0.0.2929"
}

group = 'com.skillbox.microservices'
version = '0.1.0'
sourceCompatibility = '11'

jib.to.image="post-service"
jib.to.tags = [version]

configurations {
	compileOnly {
		extendsFrom annotationProcessor
	}
}

repositories {
	mavenCentral()
}

ext {
	set('springCloudVersion', "2021.0.3")
	set('testcontainersVersion', "1.17.3")
}

dependencies {
	implementation 'org.springframework.boot:spring-boot-starter-web'
	implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
	implementation 'org.springframework.cloud:spring-cloud-starter-sleuth'
	implementation 'org.springframework.cloud:spring-cloud-sleuth-zipkin'
	implementation 'org.springframework.cloud:spring-cloud-starter-bootstrap'
	implementation 'org.modelmapper:modelmapper:3.0.0'
	implementation 'org.liquibase:liquibase-core:4.8.0'
	implementation 'org.apache.commons:commons-io:1.3.2'
	implementation 'com.amazonaws:aws-java-sdk:1.12.413'
	implementation 'commons-fileupload:commons-fileupload:1.5'
	implementation 'org.springdoc:springdoc-openapi-ui:1.6.14'
//	implementation 'org.junit.jupiter:junit-jupiter-engine:5.8.2'

	runtimeOnly 'org.postgresql:postgresql'
	compileOnly 'org.projectlombok:lombok'
	annotationProcessor 'org.projectlombok:lombok'

	// Unit test own dependencies
	testImplementation 'org.springframework.boot:spring-boot-starter-test'
	testImplementation 'org.springframework.security:spring-security-test'
	testImplementation 'org.testcontainers:junit-jupiter'
	testImplementation 'io.findify:s3mock_2.13:0.2.6'
	testImplementation 'org.jacoco:org.jacoco.report:0.8.10'
	testCompileOnly 'org.projectlombok:lombok'
	testAnnotationProcessor 'org.projectlombok:lombok'

	// integration test own dependencies
	integTestImplementation 'org.springframework.cloud:spring-cloud-contract-wiremock:2.2.8.RELEASE'
	integTestImplementation 'org.testcontainers:postgresql'
	integTestImplementation 'org.jacoco:org.jacoco.report:0.8.10'

}

dependencyManagement {
	imports {
		mavenBom "org.springframework.cloud:spring-cloud-dependencies:${springCloudVersion}"
		mavenBom "org.testcontainers:testcontainers-bom:${testcontainersVersion}"
	}
}

tasks.named('test') {
	useJUnitPlatform()
}

tasks.named('integrationTest') {
	useJUnitPlatform()
}

jacocoTestReport {
	executionData tasks.withType(Test).findAll { it.state.executed }
	executionData(file("${project.buildDir}/jacoco/test.exec"),
			file("${project.buildDir}/jacoco/integrationTest.exec"))
	reports {
		xml.enabled true
		xml.destination file("${buildDir}/jacoco/jacoco.xml")
	}
}

integrationTest.finalizedBy jacocoTestReport

test.finalizedBy jacocoTestReport

sonarqube {
	properties {
		property "sonar.projectKey", "study-microservices_post-service"
		property "sonar.organization", "study-microservices"
	}
}