#!/bin/sh

docker run --rm -d --name minio \
        --publish 9000:9000 \
        --publish 9001:9001 \
        --publish 9443:443 \
        --env MINIO_ROOT_USER="minio_admin" \
        --env MINIO_ROOT_PASSWORD="minio_admin" \
        --volume /Users/vitalijkolesnik/.minio:/root/.minio/ \
        --volume miniodata:/data \
        bitnami/minio:latest

# sudo docker run -p 443:443
# -v /home/user/.minio:/root/.minio/
# -v /home/user/data:/data minio/minio server --address ":443" /data