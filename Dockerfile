FROM openjdk:11

WORKDIR /code

COPY ./build/libs/*.jar /code/

EXPOSE 9900
CMD java -jar *.jar
