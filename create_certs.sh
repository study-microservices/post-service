#!/bin/zsh

set -euo pipefail

### Check if a directory does not exist ###
if [ ! -d "cert" ]
then
    mkdir "cert"
fi
cd cert/

openssl req -new -text -passout pass:test -subj /CN=localhost -out server.req -keyout privkey.pem
openssl rsa -in privkey.pem -passin pass:test -out server.key
openssl req -x509 -in server.req -text -key server.key -out server.crt
chmod 600 server.key
xattr -w com.docker.owner 0:999:999 server.key
