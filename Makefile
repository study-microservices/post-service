
serviceDirs := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))

all: build run

build: service

service:
	$(foreach prj, $(serviceDirs), pushd $(prj) && ./gradlew jibDockerBuild && popd &&) echo "Build complete"

run:
	docker compose up
